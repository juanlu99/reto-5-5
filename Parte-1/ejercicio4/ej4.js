"use strict";

async function getStefanoMemes() {
  let userInput = Number(
    prompt("Introduce el número de chistes que quieres recibir.")
  );
  const URL_CHUCK_NORRIS = `http://api.icndb.com/jokes/random/${userInput}?firstName=Stefano&lastName=Peraldini
  `;
  try {
    const response = await fetch(URL_CHUCK_NORRIS);
    const { value } = await response.json();
    for (let i = 0; i < userInput; i++) {
      console.log(`Joke ${i}: ${value[i].joke}`);
    }
  } catch (error) {
    console.log(error);
  }
}

getStefanoMemes();
