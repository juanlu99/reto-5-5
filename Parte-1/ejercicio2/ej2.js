"use strict";
const URL_MEMES = "https://api.imgflip.com/get_memes";

async function getMemes() {
  let userInput = Number(
    prompt("Introduzca el número de memes que quiere recibir.")
  );
  try {
    const response = await fetch(URL_MEMES);
    const { data } = await response.json();
    const memeNames = data.memes.map((meme) => meme.name);
    const memeURL = data.memes.map((meme) => meme.url);
    for (let i = 0; i < userInput; i++) {
      console.log(`Meme ${i}: ${memeNames[i]} -- ${memeURL[i]}`);
    }
  } catch (error) {
    console.log(error);
  }
}

getMemes();
