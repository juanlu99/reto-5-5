"use strict";

let userSearch = prompt("Introduzca una palabra en inglés.");

async function getDefinition(userSearch) {
  const URL_SEARCH = `https://api.dictionaryapi.dev/api/v2/entries/en/${userSearch}`;
  try {
    const response = await fetch(URL_SEARCH);
    const wordData = await response.json();
    if (!response.ok) {
      alert(`${wordData.title}.\n${wordData.message}\n${wordData.resolution}`);
    } else {
      alert(wordData[0].meanings[0].definitions[0].definition);
    }
  } catch (error) {
    console.log(error);
  }
}

getDefinition(userSearch);
