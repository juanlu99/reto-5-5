"use strict";

async function getChuckMemes() {
  let userInput = Number(
    prompt("Introduce el número de chistes que quieres recibir.")
  );
  const URL_CHUCK_NORRIS = `http://api.icndb.com/jokes/random/${userInput}`;
  try {
    const response = await fetch(URL_CHUCK_NORRIS);
    const { value } = await response.json();
    for (const joke of value) {
      console.log(joke.joke);
    }
  } catch (error) {
    console.log(error);
  }
}

getChuckMemes();
