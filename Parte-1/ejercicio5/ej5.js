"use strict";

async function getGotQuotes() {
  const userInput = Number(
    prompt("Introduce el número de frases que quieras recibir.")
  );
  const URL_GOT = `https://game-of-thrones-quotes.herokuapp.com/v1/random/${userInput}`;
  try {
    const response = await fetch(URL_GOT);
    const quotes = await response.json();
    const frag = document.createDocumentFragment();
    if (userInput > 1) {
      for (const quote of quotes) {
        const li = document.createElement("li");
        li.innerHTML = `<b>${quote.character.name}</b> -- ${quote.sentence}`;
        frag.append(li);
      }
    } else {
      const li = document.createElement("li");
      li.innerHTML = `<b>${quotes.character.name}</b> -- ${quotes.sentence}`;
      frag.append(li);
    }
    const ul = document.querySelector("ul");
    ul.append(frag);
  } catch (error) {
    console.log(error);
  }
}

getGotQuotes();
