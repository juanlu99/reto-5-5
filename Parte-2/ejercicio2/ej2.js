"use strict";

function getRandomInt(max) {
  return Math.floor(Math.random() * max + 1);
}

async function dogPic() {
  try {
    const response = await fetch("https://random.dog/woof.json");
    const { url } = await response.json();
    const ul = document.querySelector("ul");
    const li = document.createElement("li");
    const frag = document.createDocumentFragment();
    li.innerHTML = `Enjoy your dog pic!! <img src="${url}" alt="Dog-Pic-Woof" "height="325px" width="325px">`;
    frag.append(li);
    ul.append(frag);
  } catch (error) {
    console.log(error);
  }
}

async function catPic() {
  try {
    const response = await fetch("https://aws.random.cat/meow");
    const { file } = await response.json();
    const ul = document.querySelector("ul");
    const li = document.createElement("li");
    const frag = document.createDocumentFragment();
    li.innerHTML = `Enjoy your cat pic!! <img src="${file}" alt="Cat-Pic-Meow" "height="325px" width="325px">`;
    frag.append(li);
    ul.append(frag);
  } catch (error) {
    console.log(error);
  }
}

async function foxPic() {
  try {
    const response = await fetch("https://randomfox.ca/floof/");
    const { image } = await response.json();
    const ul = document.querySelector("ul");
    const li = document.createElement("li");
    const frag = document.createDocumentFragment();
    li.innerHTML = `Enjoy your fox pic!! <img src="${image}" alt="Fox-Pic-WhatDoesTheFoxSay" "height="325px" width="325px">`;
    frag.append(li);
    ul.append(frag);
  } catch (error) {
    console.log(error);
  }
}

let numOfAnimals = Number(prompt("¿Cuantas fotos quiere ver?"));

for (let i = 0; i < numOfAnimals; i++) {
  let animalSelector = getRandomInt(3);
  console.log(animalSelector);
  switch (animalSelector) {
    case 1:
      dogPic();
      break;
    case 2:
      catPic();
      break;
    case 3:
      foxPic();
      break;
  }
}
