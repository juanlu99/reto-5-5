'use strict';
const arrayPlayers = [];
let arrayAlivePlayers = [];
const deathSkull = 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Death_skull.svg/1061px-Death_skull.svg.png';
const userList = document.querySelector('ul.userlist');
const loading = document.querySelector('li.loading');
const announcer = document.querySelector('h2.announcer');
const roundButton = document.querySelector('button.next-round');
let numberOfPlayers = Number(prompt('Introduzca el número de jugadores!'));
let numberOfMaxRounds = Number(prompt('Introduzca el número de rondas!'));
let currentRound = 0;

function getRandomInt0toMax(max) {
  return Math.floor(Math.random() * max);
}
function render() {
  userList.innerHTML = '';
  const frag = document.createDocumentFragment();
  for (let i = 0; i < arrayPlayers.length; i++) {
    const { firstName, lastName, titleName, picture, city, country, yOfB, death } = arrayPlayers[i];
    const userLi = document.createElement('li');
    if (!death) {
      userLi.innerHTML = `
      <article>
        <header>
          <img src="${picture}" alt="${titleName}. ${firstName}, ${lastName}">
          <h1>${titleName}. ${firstName} ${lastName}</h1>
        </header>
        <p>${city} (${country}), ${yOfB}</p>
      </article>`;
      userLi.classList.add('alive');
    } else if (death) {
      userLi.innerHTML = `<img src="${deathSkull}" width="188px" height="188px">`;
    }
    frag.append(userLi);
  }
  loading.remove();
  userList.append(frag);
  announcer.textContent = `Ronda ${currentRound}: ${document.querySelectorAll('li.alive').length} JUGADORES VIVOS`;
}
const getUsers = async (numUsers) => {
  try {
    const response = await fetch(`https://randomuser.me/api/?results=${numUsers}`);
    const { results } = await response.json();
    for (const result of results) {
      const { name, picture, location, dob } = result;
      const yearOfBirth = new Date(dob.date).getFullYear();
      let player = {
        firstName: name.first,
        lastName: name.last,
        titleName: name.title,
        picture: picture.large,
        city: location.city,
        country: location.country,
        yOfB: yearOfBirth,
        death: false,
      };
      arrayPlayers.unshift(player);
      arrayAlivePlayers.unshift(player);
    }
  } catch (error) {
    console.log(error);
  }
  render();
};
function deathButton() {
  let numberOfKills = Number();
  if (currentRound === numberOfMaxRounds - 1) {
    numberOfKills = arrayAlivePlayers.length - 1;
  } else if (currentRound === numberOfMaxRounds - 2) {
    numberOfKills = arrayAlivePlayers.length - 2;
  } else {
    numberOfKills = getRandomInt0toMax(arrayAlivePlayers.length / 2);
  }
  currentRound++;
  for (let i = 0; i < numberOfKills; i++) {
    if (arrayAlivePlayers.length > 1) {
      let idSelected = getRandomInt0toMax(arrayAlivePlayers.length);
      arrayAlivePlayers[idSelected].death = true;
      arrayAlivePlayers.splice(idSelected, 1);
    }
  }
  if (arrayAlivePlayers.length === 2) {
    alert(`TENEMOS FINALISTAS!!!! ${arrayAlivePlayers[0].firstName} VS ${arrayAlivePlayers[1].firstName}`);
  } else if (arrayAlivePlayers.length === 1) {
    alert(`${arrayAlivePlayers[0].firstName} HA GANADO!!`);
    currentRound = numberOfMaxRounds;
    roundButton.remove();
  }
  render();
}

getUsers(numberOfPlayers);
roundButton.addEventListener('click', deathButton);
