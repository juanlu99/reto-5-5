"use strict";
const URL_MEMES = "https://api.imgflip.com/get_memes";

async function getMemes() {
  let userInput = Number(
    prompt("Introduzca el número de memes que quiere recibir.")
  );
  try {
    const response = await fetch(URL_MEMES);
    const { data } = await response.json();
    const memeNames = data.memes.map((meme) => meme.name);
    const memeURL = data.memes.map((meme) => meme.url);
    const ul = document.querySelector("ul");
    const frag = document.createDocumentFragment();
    for (let i = 0; i < userInput; i++) {
      const li = document.createElement("li");
      li.innerHTML = `${memeNames[i]}<img src="${memeURL[i]}" height="250px" width="250px">`;
      frag.append(li);
    }
    ul.append(frag);
  } catch (error) {
    console.log(error);
  }
}

getMemes();
