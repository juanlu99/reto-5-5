"use strict";

//Dirigidas por Hayao Miyazaki

(async function directedBy() {
  try {
    const response = await fetch("https://ghibliapi.herokuapp.com/films");
    const filmData = await response.json();
    const ul = document.querySelector(".directed-by");
    const frag = document.createDocumentFragment();
    for (const film of filmData) {
      if (film.director === "Hayao Miyazaki") {
        const li = document.createElement("li");
        li.innerHTML = `Título: ${film.title} || Título Original: ${film.original_title}
        <img src="${film.movie_banner}" alt="movie-banner" height="250px">`;
        frag.append(li);
      }
      ul.append(frag);
    }
  } catch (error) {
    console.log(error);
  }
})();

//Del anterior, score > 60 y posteriores a 1980

(async function scoreYear() {
  try {
    const response = await fetch("https://ghibliapi.herokuapp.com/films");
    const filmData = await response.json();
    const ul = document.querySelector(".score-year");
    const frag = document.createDocumentFragment();
    for (const film of filmData) {
      if (
        film.director === "Hayao Miyazaki" &&
        parseInt(film.rt_score) > 80 &&
        parseInt(film.release_date) > 1980
      ) {
        const li = document.createElement("li");
        li.innerHTML = `Título: ${film.title} || Título Original: ${film.original_title}
        <img src="${film.movie_banner}" alt="movie-banner" height="250px">`;
        frag.append(li);
      }
    }
    ul.append(frag);
  } catch (error) {
    console.log(error);
  }
})();

//Gatos en Peliculas

(async function catInMovies() {
  try {
    const response = await fetch(
      "https://ghibliapi.herokuapp.com/species/603428ba-8a86-4b0b-a9f1-65df6abef3d3"
    );
    const { people } = await response.json();
    const ul = document.querySelector(".cats");
    const frag = document.createDocumentFragment();
    for (const cat of people) {
      try {
        const catResponse = await fetch(cat);
        const { name, films } = await catResponse.json();
        for (const film of films) {
          const filmResponse = await fetch(film);
          const { title, original_title } = await filmResponse.json();
          const li = document.createElement("li");
          li.innerHTML = `Film: ${title} || ${original_title} --- 
          <b>Cat: ${name}</b>`;
          frag.append(li);
        }
      } catch (error) {
        console.log(error);
      }
    }
    ul.append(frag);
  } catch (error) {
    console.log(error);
  }
})();

//Peliculas y sus climas

(async function movieClimate() {
  try {
    const response = await fetch("https://ghibliapi.herokuapp.com/locations");
    const locationData = await response.json();
    const ul = document.querySelector(".movie-climate");
    const frag = document.createDocumentFragment();
    for (const location of locationData) {
      for (const film of location.films) {
        try {
          const filmResponse = await fetch(film);
          const { title, original_title } = await filmResponse.json();
          const li = document.createElement("li");
          li.innerHTML = `Localización: ${location.name} --- 
          <b>Clima: ${location.climate}</b> --- 
          Película: ${title} || ${original_title}`;
          frag.append(li);
        } catch (error) {
          console.log(error);
        }
      }
    }
    ul.append(frag);
  } catch (error) {
    console.log(error);
  }
})();
