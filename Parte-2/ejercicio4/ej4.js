"use strict";

async function getRandomWeb() {
  try {
    const animeResponse = await fetch(
      "https://anime-facts-rest-api.herokuapp.com/api/v1/"
    );
    const { data } = await animeResponse.json();
    const animeUl = document.querySelector("ul");
    const frag = document.createDocumentFragment();
    for (const anime of data) {
      const animeImg = anime.anime_img;
      const animeName = anime.anime_name;
      const animeLi = document.createElement("li");
      animeLi.innerHTML = `
      <h2>${animeName}</h2>
      <img src="${animeImg}" alt="anime-picture" height="300px">`;
      try {
        const factResponse = await fetch(
          `https://anime-facts-rest-api.herokuapp.com/api/v1/${animeName}`
        );
        const { data } = await factResponse.json();
        const factP = document.createElement("p");
        for (let i = 0; i < 3; i++) {
          const factPhrase = data[i].fact;
          factP.append(factPhrase);
        }
        animeLi.append(factP);
      } catch (error) {
        console.log(error);
      }
      frag.append(animeLi);
      animeUl.append(frag);
    }
  } catch (error) {
    console.log(error);
  }
}

getRandomWeb();
